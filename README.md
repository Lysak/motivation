Motivation
========
Your age.

With ubuntu font and default chrome incognito mode color

For Chrome

![](chrome_motivation_lysak.png)

For Firefox add add-on from [addons.mozilla.org](https://addons.mozilla.org/ru/firefox/addon/motivation-new-tab/ "https://addons.mozilla.org/ru/firefox/addon/motivation-new-tab/")

![](firefox_motivation_lysak.png)
